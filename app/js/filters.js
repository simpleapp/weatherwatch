'use strict';

/* Filters */

angular.module('SimpleWeather.filters', []).filter('singleDigit', function(){
	return function(text)
	{
		if (text[0]== '0') return text[1];
		return text;
	}
});
