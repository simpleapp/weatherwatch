'use strict';

/* Controllers */

function WeatherCtrl ($scope,WeatherService){
  	$scope.date = Date.now();


  	var updateTime = function()
  	{
  		console.log("update time");
  		var d = new Date();
  		$scope.now_hour = d.getHours() + d.getMinutes()*1./60 + d.getSeconds()*1./3600;
      $scope.now_minute = d.getMinutes() + d.getSeconds()*1./60;
      $scope.now_second = d.getSeconds();
  		window.setTimeout(function(){
  			updateTime();
  			$scope.$digest();
  		}, 1000);
  	};

  	updateTime();

  	$scope.daychoices = [];
  	$scope.Math = window.Math;
  	var watch_width = window.getComputedStyle(window.document.getElementById('morning_watch')).getPropertyCSSValue("width").getFloatValue(CSSPrimitiveValue.CSS_PX);
  	console.log( watch_width);
  	$scope.radius = watch_width*0.5;
    $scope.radius_factor = 0.7;

  	var tmpDate = null;
  	for (var i = 0; i< 10 ; i++)
  	{
  		tmpDate = new Date();
  		tmpDate.setDate(tmpDate.getDate() + i);
  		$scope.daychoices.push({"name":tmpDate , delta:i});
  	}


	$scope.day = $scope.daychoices[0];
  	$scope.location = null;

  	$scope.$watch('day', function(choice){
  		console.log("day changed :");
  		console.log(choice);
  		if (choice==null) return;
  		$scope.deltaday = choice.delta;
  		refreshWeather();
  	});

  	$scope.$watch('location', function(){
  		console.log("location changed");
  		console.log($scope.location);
  		refreshWeather();
  	});


  	var onLocationFound = function(location)
  	{
  		console.log("onLocationFound");
  		$scope.location = location;
  		$scope.$digest();
  	}

  	var refreshWeather = function()
  	{
  		var location = $scope.location;
  		if (location == null) return;
  		$scope.now_date = new Date();
  		$scope.morning_hourly = [];
  		$scope.afternoon_hourly = [];
  		var selected_date = new Date($scope.now_date.getFullYear(), $scope.now_date.getMonth(), $scope.now_date.getDate() + $scope.deltaday);
  		WeatherService.getByLatLon(location.coords.latitude,location.coords.longitude,Math.round(selected_date/1000)).then(
  			function (response){
  				console.log("ok ");
  				console.log(response);
  				console.log(response.data["hourly"]);
	  			var hourlyForecast = response.data.hourly.data;
	  			console.log('hourlyForecast :' );
	  			console.log(hourlyForecast);
	  			for (var i = 0; i < hourlyForecast.length && i < 12; i++)
	  			{
	  				console.log("push icon : "+ hourlyForecast[i].icon);
	  				$scope.morning_hourly.push({date:new Date(hourlyForecast[i].time * 1000), icon:hourlyForecast[i].icon, temperature:hourlyForecast[i].temperature});
	  			}
		  		for (var i = 12; i < hourlyForecast.length && i < 24; i++)
		  		{
		  			console.log("push icon : "+ hourlyForecast[i].icon);
		  			$scope.afternoon_hourly.push({date:new Date(hourlyForecast[i].time * 1000), icon:hourlyForecast[i].icon, temperature:hourlyForecast[i].temperature});
		  		}
	  		},
	  		function (error){
	  			console.log("oops : "+error);
	  		}
  		);
  	};
  	navigator.geolocation.getCurrentPosition(onLocationFound);
}
