'use strict';


// Declare app level module which depends on filters, and services
var SimpleWeather = angular.module('SimpleWeather', ['SimpleWeather.filters', 'SimpleWeather.services', 'SimpleWeather.directives']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/weather', {templateUrl: 'partials/weather.html', controller: 'WeatherCtrl'});
    $routeProvider.otherwise({redirectTo: '/weather'});
  }]);
